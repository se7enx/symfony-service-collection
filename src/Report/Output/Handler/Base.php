<?php

namespace App\Report\Output\Handler;

abstract class Base
{
    protected $outputFile;
    protected $fileHandler;

    public function getOutputFile(): ?string
    {
        return $this->outputFile;
    }

    public function output(iterable $data): void
    {
        $this->fileHandler = fopen($this->outputFile, 'w');

        foreach ($data as $row) {
            $this->handleRow($row);
        }

        fclose($this->fileHandler);
    }

    protected function handleRow(array $row): void
    {
    }
}