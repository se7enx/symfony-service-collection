<?php

namespace App\Report\Output\Handler;

class Csv extends Base
{
    protected $outputFile = 'var/report.csv';
    protected $hasHeaders = false;

    protected function handleRow(array $row): void
    {
        if ($this->hasHeaders === false) {
            $this->hasHeaders = true;
            $this->handleRow(array_keys($row));
        }

        fputcsv($this->fileHandler, $row);
    }
}