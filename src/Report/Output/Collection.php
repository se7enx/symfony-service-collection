<?php

namespace App\Report\Output;

use Exception;
use App\Report\Output\Handler\Base as Handler;

class Collection
{
    protected $handlers = [];

    public function __construct(iterable $handlers)
    {
        //var_dump(count($handlers)); exit();
        foreach ($handlers as $handler) {
            $identifier = $this->getHandlerIdentifier($handler);
            $this->handlers[$identifier] = $handler;
        }
    }

    public function getAvailableHandlers(): array
    {
        return array_keys($this->handlers);
    }

    public function get(string $identifier): Handler {
        if (isset($this->handlers[$identifier]) === false) {
            throw new Exception('"' . $identifier . '" is invalid handler.');
        }

        return $this->handlers[$identifier];
    }

    protected function getHandlerIdentifier(Handler $handler): string
    {
        $class = get_class($handler);

        return $class;
    }
}